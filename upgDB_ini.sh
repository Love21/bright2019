## Variable####
SCRIPTDIR=$1 #directory where we have our SQL-scripts
DBUSER=$2
DBHOST=$3
DBNAME=$4
DBPASS=$5

# Remove zeroes and extract the number from the filename
regex='^0*([0-9]+)'

# Database version
version=$(mysql -u $DBUSER -p$DBPASS -e "use $DBNAME; SELECT version from versionTable;")
version=`echo $version | cut -d' ' -f2`

# Most recent  sql script version in the sql scripts folder
latest_script=`ls $SCRIPTDIR | sort -n | grep -Eo $regex | tail -n 1`
latest_filename=`ls $SCRIPTDIR | sort -n | tail -n 1`

# Upgrade the database  when there is a new sql script  in the directory 
	if [[ $latest_script -gt $version ]] 
	then
    # Sort list of SQL-scripts
    files="$(ls -1 $SCRIPTDIR | sort -n)"
    # Executing scripts
   for filename in $files
   do
   # Match the pattern defined in regular expression
        if [[ $filename =~ $regex ]]
        then
            # save the file version in  to a variable to be compare with version store in versionTable
            my_script_version=${BASH_REMATCH[1]}

            # Compare sql script version with MySQL version, if our version < number, than we need to upgrade
            if [[ $version -lt $my_script_version ]]
            then
                echo "'$filename', database version $my_script_version is being executed"
                # Execute SQL-script inside the database
                mysql  -h localhost -u $DBUSER -p$DBPASS $DBNAME  < $SCRIPTDIR/$filename
                mysql  -u $DBUSER -p$DBPASS $DBNAME -e "UPDATE versionTable SET version=$my_script_version;"
           else
               echo "$filename won't be executed"  
	   fi
        fi
    done
    new_version=$(mysql -u $DBUSER -p$DBPASS -e "use $DBNAME; SELECT version from versionTable;")
    new_version=`echo $new_version | cut -d' ' -f2`
    echo "Database has been upgraded to version "$new_version

# No update is needed if the version stored in the version Table is greater or equal to the sql script file versio number
else
    echo "No update required: Current Database version is $version. $latest_filename is the last file in the directory"
fi

