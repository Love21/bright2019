# Upgrade database version with bash script

HOW TO RUN THE BASH SCRIPT


1. Installed mysql 5.7 
	yum/apt-get/brew to install mysql db server
2. Start mysql service
	service mysqld start  or /etc/init.d/mysql start
3. Set up the login credential: user DB password
	grep 'A temporary password' /var/log/mysqld.log |tail -1  # to get a tempory password
	mysql -u root -p

3. Clone the repository
	git clone https://Love21@bitbucket.org/Love21/bright2019.git
4. Switch to the directory bright2019
	cd bright2019
5. Create a database environment with the dump script 'mydb.sql'
	mysql -h localhost -u root -p < mydb.sql
6. Switch to the sql scripts folder and fill it with it with sql script files
	cd sql_scripts
7. Switch back to your working directory "/bright2019/
	cd ..
8. Run the following command to execute the bash cript "upgDB_ini.sh"
	./upgDB_ini.sh "./sql_scripts" "root" "localhost" "ecsdb" "USERDB Password"


 
To check the database environment:

a- Login to MySQL 
	mysql -h localhost -u root -p 
b- Check the database
	SHOW databases;  # lists databases in MySQL
	USE ecsdb; # ecsdb is the name of database set by default for this test. it can be changed in mysql.sql
	SELECT * FROM versionTable; # 


